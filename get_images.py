"""
get_images.py - Obtains images using flickr API.

UVA CS 1120 - Spring 2016
Problem Set 1
"""

import os
import argparse
import requests
from PIL import Image
from PIL import ImageOps

API_KEY = '6754bb882cbec828c9eb07da61ceae88'
SEARCH_API_URL = 'https://api.flickr.com/services/rest/'

def get_args():
    """Process command line arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument('search_text', type=str, nargs=None, default=None,
                        help='search text')
    parser.add_argument('-l', '--limit', type=int, default=None,
                        help='limit for the result')
    parser.add_argument('-d', '--directory', type=str, default=os.getcwd(),
                        action='store', help='the directory where the images will be stored')
    return parser.parse_args()

def save_image(image, savepath):
    """Saves the image a file named using the image's source url within the savepath directory."""
    if not os.path.isdir(savepath):
        os.mkdir(savepath)

    fname = os.path.join(savepath, os.path.basename(image.url))
    with open(fname, 'wb') as ifile:
        ifile.write(image.content)
        ifile.close()

    img = Image.open(fname)
    size = 28, 21
    #img.thumbnail(size, Image.ANTIALIAS)
    img = ImageOps.fit(img,size,Image.ANTIALIAS)
    filename, file_extension = os.path.splitext(fname)
    img.save(filename + '.gif')

def find_images(search_text, limit):
    """
    Returns a list of images that match the provided search_text.
    Up to limit images will be returned.
    """
    params = {
        'method': 'flickr.photos.search',
        'api_key': API_KEY,
        'text': search_text,
        'safe_search': 1, # please don't change this - only tasteful photomosaics are desired!
        'content_type': 1,
        'per_page': limit,
        'format': 'json',
        'nojsoncallback': 1,
        'sort': 'relevance'
    }

    responses = requests.get(SEARCH_API_URL, params=params).json()
    return responses['photos']['photo']

def download_images(search_text, output_dir, limit=100):
    """
    Download and store up to limit images that match the search_text.
    Images are stored in the output_dir.
    """

    img_url = 'https://farm{farm_id}.staticflickr.com/{server_id}/{id}_{secret}.jpg'

    print("Downloading images matching '" + search_text + "'...")
    images = find_images(search_text, limit)
    print("Found " + str(len(images)) + " images...")

    for res in images:
        params = {
            'farm_id': res['farm'],
            'server_id': res['server'],
            'id': res['id'],
            'secret': res['secret']
        }

        url = img_url.format(**params)
        image = requests.get(url)
        save_image(image, output_dir)
        print("Saving image " + url)

def main():
    """This function runs when python get_images.py is executed."""
    args = get_args()
    download_images(args.search_text, args.directory, args.limit)

if __name__ == '__main__':
    main()
