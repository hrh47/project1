"""
mosaic.py

UVA CS 1120 - Spring 2016
Problem Set 1
"""
import glob
from graphics import *

### Functions for making colors and extracting their components.

def make_color(red, green, blue):
    """
    We represent a color as a list of three numbers corresponding to its
    red, green and blue components.
    """
    return [red, green, blue]

def get_red(color):
    """Return the red component of the input color."""
    return color[0]

def get_green(color):
    """Return the green component of the input color."""
    return color[1]

def get_blue(color):
    """Return the blue component of the input color."""
    return color[2]

# Colors are represented as lists of red, green and blue light intensities.
# Here are definitions of some common colors:

WHITE = make_color(255, 255, 255)
BLACK = make_color(0, 0, 0)
RED = make_color(255, 0, 0)
GREEN = make_color(0, 255, 0)
BLUE = make_color(0, 0, 255)
YELLOW = make_color(255, 255, 0)
PURPLE = make_color(102, 51, 153)

### Functions for combining colors.

def add_color(color1, color2):
    """Returns a new color that is equal to the sum of the two input colors."""
    return make_color(
        get_red(color1) + get_red(color2),
        get_green(color1) + get_green(color2),
        get_blue(color1) + get_blue(color2))

def sum_colors(color_list):
    """"Returns a new color that is the sum of all the colors in the input list of colors."""
    if color_list is []:
        # If there are no colors to sum, return black.
        return make_color(0, 0, 0)
    else:
        # Otherwise, add the first color in the list to the
        # sum of the rest of the colors in the list.
        return add_color(color_list[0], sum_colors(color_list[1:]))

#def sum_colors_alterate(color_list):
    #"""Returns a new color that is the sum of all the colors in the input list of colors.

    #This implements the same function as sum_colors above, but with a one-liner.
    #We'll cover "reduce" (also known as "fold") later in this course.
    #"""
    #return reduce(add_color, color_list, make_color(0, 0, 0))


###

def find_best_match(sample_color, tiles, tile_colors, closer_color):
    """
        Given a sample color, find the tile image that is closest to
        that sample color according to closer_color.
    """
    if tiles is []:
        print("Error: no tiles to match?")
        return None
    elif len(tiles) is 1:
        return [tiles[0], tile_colors[0]]
    else:
        [a_tile, a_color] = [tiles[0], tile_colors[0]]
        [b_tile, b_color] = find_best_match(sample_color, tiles[1:], tile_colors[1:], closer_color)
        if closer_color(sample_color, a_color, b_color):
            return [a_tile, a_color]
        else:
            return [b_tile, b_color]

def make_photomosaic(base_filename, tile_directory, color_comparator):
    """
    Creates a photomosaic that looks like the image in base_filename but is made up of tiles
    from the GIF images in tile_directory.
    """

    # First, we load the base image.
    base = Image(Point(0, 0), base_filename)
    owidth, oheight = base.getWidth(), base.getHeight()

    # Next, we find all of the tiles files.
    tile_files = glob.glob(tile_directory + "/*.gif")

    # Cannot make a photomosaic without any tiles:
    if not tile_files:
        print("No .gif files found in " + tile_directory)
        return

    # Next, load all of the tile images.
    tile_images = [Image(Point(0, 0), fname) for fname in tile_files]

    twidth = tile_images[0].getWidth()
    theight = tile_images[0].getHeight()

    if owidth < twidth or oheight < theight:
        print("Original image is smaller than the tiles!")
        return

    def image_average_region_color(image, localx, localy, localdx, localdy):
        """
        Returns the average color of a rectangular sub-region of an image.  This is computed as the
        average color for all pixels between [x,y] and (x+dx, y+dy) (not including the edges).
        """
        total = [0, 0, 0]
        numpix = 0
        for localxp in range(localx, localx + localdx):
            for localyp in range(localy, localy + localdy):
                pixel = image.getPixel(localxp, localyp)
                total = add_color(total, pixel)
                numpix += 1
        assert numpix == localdx * localdy
        return make_color(get_red(total) / numpix, 
                          get_green(total) / numpix, 
                          get_blue(total) / numpix)

    def image_average_color(image):
        """Returns the average color over the input image."""
        return image_average_region_color(image, 0, 0, image.getWidth(), image.getHeight())

    tile_colors = [image_average_color(tile) for tile in tile_images]

    win = GraphWin("Photomosaic", owidth, oheight)

    for iteratorx in range(owidth // twidth):
        for iteratory in range(oheight // theight):
            sample_color = image_average_region_color(base,
                                                      iteratorx * twidth, iteratory * theight,
                                                      twidth, theight)
            [best_image, best_color] = find_best_match(sample_color, tile_images, tile_colors,
                                                       color_comparator)
            pasted_image = best_image.clone()
            pasted_image.move(iteratorx * twidth, iteratory * theight)
            pasted_image.draw(win)

    win.getMouse() # Pause to view result
    win.close()    # Close window when done

    return

### Some handy procedures for displaying colors and images

def show_color(color):
    """Displays a window containing an example of the given color."""
    win = GraphWin("Showing The Colors", 640, 480)
    pcircle = Circle(Point(320, 240), 200)
    rgb = color_rgb(get_red(color), get_green(color), get_blue(color))
    pcircle.setFill(rgb)
    pcircle.draw(win)
    text1 = Text(Point(320, 20), "Click to close this window.")
    text1.draw(win)
    text2 = Text(Point(320, 460), str(color))
    text2.draw(win)
    win.getMouse() # Pause to view result
    win.close()    # Close window when done

def show_image(filename):
    """Displays a window showing the image contained in the filename input."""
    img = Image(Point(0, 0), filename)
    width = img.getWidth()
    height = img.getHeight()
    win = GraphWin("Showing " + filename, width, height)
    img.move(width / 2, height / 2) # center the image
    img.draw(win)
    win.getMouse() # Pause to view result
    win.close()    # Close window when done



